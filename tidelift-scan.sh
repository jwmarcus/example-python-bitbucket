#!/usr/bin/env sh

# Tidelift GitLab integration script
# Author: jwmarcus
# License: MIT

# For more information about setting these values, see the
# documentation at https://tidelift.com/docs/subscriber/api
TEAM="${TL_TEAM:-please-set-team}"
PROJECT="${TL_PROJECT:-please-set-project}"
BRANCH="${BITBUCKET_BRANCH:-master}"
TOKEN="${TL_TOKEN:-please-set-api-token}"

# Build headers and URIs to be used later
HEADER="Authorization: Bearer repository/$TOKEN"
BASE_URI="https://api.tidelift.com/subscriber/$TEAM/$PROJECT"
SUBMIT_URI="$BASE_URI/manifest/upload?branch=$BRANCH"

# Submit the relavent file(s) for this project.
# Receive status code directly, store response JSON into a file.
echo "[INFO] Submitting manifest files to Tidelift"
SUBMIT_CODE=$(curl -s -o submit_response.temp \
    -w "%{http_code}" -X POST \
    -F "files[]=@requirements.txt" \
    -H "$HEADER" $SUBMIT_URI)
if [ $SUBMIT_CODE != "201" ]; then
  echo "[CRIT] Submission to Tidelift failed. Code: $SUBMIT_CODE"
  cat submit_response.temp
  exit 1
fi

# Parse out the revision number from the submission response
REVISION=$(cat submit_response.temp | jq -r ".revision")
STATUS_URI="$BASE_URI/$REVISION/status"
echo "[INFO] Revision received, revision=$REVISION"

# If we query too quickly, we will get a 404. Therefore, wait until
# a new scan has begun before polling the endpoint for valid JSON.
while true; do
  STATUS_CODE=$(curl -s -o /dev/null -w "%{http_code}" -H "$HEADER" $STATUS_URI)

  if [ $STATUS_CODE == 200 ]; then
    while true; do
      # TODO: Remove this call by storing the json to file and keeping the status
      STATUS_RES=$(curl -s -H "$HEADER" $STATUS_URI)
      SCAN_STATUS=$(echo $STATUS_RES | jq -r ".status")

      if [ $SCAN_STATUS == "failure" ]; then
        exit 1
      elif [ $SCAN_STATUS == "error" ]; then
        exit 1
      elif [ $SCAN_STATUS == "success" ]; then
        exit 0
      fi

      echo "[INFO] Waiting for scan to complete. Status:$SCAN_STATUS"; sleep 5
    done
  fi
  echo "[INFO] Waiting for confirmed submission. Code:$STATUS_CODE"; sleep 3

done
